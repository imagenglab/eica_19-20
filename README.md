### README ###
"Elaborazione delle Immagini per la Chirurgia Assistita", repository class 2019-2020

Notebook_0: Introduction to Python

Notebook_1: Introduction to image procesing in Python

Notebook_2: Fourier

Notebook_3: Convolution

Notebook_4: Dicom

Notebook_5: Masking NRRD file

Notebook_6: Digitally Reconstructed Radiography

Notebook_7: Ultrasound dicom

Notebook_8: Segmentation

Notebook_9: Morphological filters

Notebook_10: MABS

Notebook_11: Segmentation metrics

Notebook_12: Features

Notebook_13: Image segmentation

Notebook_14: Triangulation

Notebook_15: Marching cubes

Notebook_16: Landmark registration

