#!/usr/bin/env python

import numpy as np
import pfm
import sys
import scipy.misc

pfm_fn = sys.argv[1]

img = pfm.load_pfm(pfm_fn).T
scipy.misc.imsave(pfm_fn.split(".")[0]+".tiff", img)

